<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row headerbg">
		<div class="col-md-2">
			<img src="/assets/images/axis_logo_tbg.png" alt="logo" width="96" height="52" />
		</div>
		<div class="col-md-10">
			<h3>Axis Labs - Pathology Lab Reporting</h3>
		</div>
	</div>
	<div class="row" style="padding-top:20px;padding-left:15px;">
		<div class="lead">Login to Lab Operator Main</div>
		<?php 
			if($this->session->flashdata('errors')) {
				echo '<div class="text-danger">'.$this->session->flashdata('errors').'</div>';
			}
		?>
	</div>
	<div class="row well-lg" style="padding-top:30px">
	<?php 
		echo form_open('login_operator_con/process_login_operator', ['class' => 'form-horizontal', 'method' => 'post', 'role' => 'form']);
			echo '<div class="form-group">'."\n";
				echo form_label('Email','staff_email',['class' => 'control-label col-md-2']);
				echo '<div class="col-md-10">';
					echo form_input(['class' => 'form-control', 'type' => 'text','name' => 'staff_email','value' => $staff_email,'placeholder' => 'Enter your email address','style' => 'width:50%']);
				echo "</div>\n";
			echo "</div>\n";
			echo '<div class="form-group">'."\n";
				echo form_label('Password','staff_password',['class' => 'control-label col-md-2']);
				echo '<div class="col-md-10">';
					echo form_password(['class' => 'form-control','type' => 'password','name' => 'staff_password','value' => '','placeholder' => 'Enter your password','style' => 'width:50%'],['style' => 'width:50%']);
				echo "</div>\n";
			echo "</div>\n";
			echo '<div class="form-group">'."\n";
				echo '<div class="col-md-offset-2 col-md-10">';
					echo form_input(['class' => 'btn btn-primary btn-sm','name' => 'get_form','type' => 'submit','value' => 'Login']);
				echo "</div>\n";
			echo "</div>\n";
		echo form_close();
	?>
</div>
