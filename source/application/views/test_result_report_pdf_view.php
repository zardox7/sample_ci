<?php defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Axis Labs - Pathology Lab Reporting System</title>
</head>
<body>
<h1>Axis Labs - Pathology Lab Reporting</h1>
<p><h2>Lab Results for <?php echo $name_first.' '.$name_last ?></h2></p>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
	<td>
		<h3>Patient Information</h3>
		<table border="0" cellspacing="5" cellpadding="2">
		<tr>
			<td style="width:100px">Date birth</td>
			<td><?php echo date('M j, Y',strtotime($date_birth)); ?></td>
		</tr>
		<tr>
			<td>Address</td>
			<td>
				<?php echo $address_block; ?>
			</td>
		</tr>
		</table>
	</td>
	<td>
		<h3>Lab Information</h3>
		<table border="0" cellspacing="5" cellpadding="2">
		<tr><td style="width:100px">Lab Name</td> <td><?php echo $lab_name.(! empty($lab_location) ? ' ('.$lab_location.')' : '') ?></td></tr>
		<tr><td>Lab Operator</td> <td><?php echo $staff_name_first.' '.$staff_name_last ?></td></tr>
		<tr><td>Lab Operator Location</td> <td><?php echo $staff_location ?></td></tr>
		</table>
	</td>
</tr>
</table>
<h3>Test Results</h3>
<table border="0" cellspacing="5" cellpadding="2">
<tr>
	<td style="width:100px">Test Performed</td> 
	<td><?php echo $test_code.(! empty($test_code) ? ' - ' : '').$test_name ?></td>
</tr>
<tr>
	<td>Date Test Requested</td> 
	<td width="80%"><?php echo date('d-m-Y',strtotime($date_test_result_request)) ?></td>
</tr>
<tr>
	<td>Date Test Completed</td> 
	<td><?php echo date('d-m-Y',strtotime($date_test_result_service)) ?></td>
</tr>
</table>
<?php if( empty($test_result_diagnosis) ): ?>
<div style="color:red;">There are no test results to report.</div>
<?php else: ?>
<h3>Diagnosis</h3><br>
<?php echo $test_result_diagnosis ?>
<?php  endif; ?>

</body>
</html>