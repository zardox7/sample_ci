<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row headerbg">
		<div class="col-md-2">
			<img src="/assets/images/axis_logo_tbg.png" alt="logo" width="96" height="52" />
		</div>
		<div class="col-md-10">
			<h3>Axis Labs - Pathology Lab Reporting</h3>
		</div>
	</div>
	<div class="row" style="padding-top:20px;padding-left:15px;">
		<div class="lead">Patient - Lab Test Result</div>
		<?php 
			if($this->session->flashdata('errors')) {
				echo '<div class="text-danger">'.$this->session->flashdata('errors').'</div>';
			}
		?>
	</div>
	<div class="row well-lg" style="padding-top:30px">
		<table class="table table-bordered table-condensed"><tbody>
		<tr>
			<td valign="top">Patient Name </td>
			<td><?php echo $patient->name_first.' '.$patient->name_last; ?></td>
		</tr>
		<tr>
			<td width="30%">Email</td>
			<td><?php echo mailto($patient->email); ?></td>
		</tr>
		<tr>
			<td valign="top">Gender</td>
			<td><?php echo strtoupper($patient->gender)=='M'? 'Male':'Female'; ?></td>
		</tr>
		<tr>
			<td valign="top">Date of birth (mm-dd-yyyy)</td>
			<td><?php echo date('M j, Y',strtotime($patient->date_birth)); ?></td>
		</tr>
		<tr>
			<td width="30%">Pass Code</td>
			<td><?php echo $patient->patient_pass_code; ?></td>
		</tr>
		<tr>
			<td width="30%">Address</td>
			<td><?php 
				$this->load->helper('MC');
				echo address_block([
					'address_1' => $patient->address_1,
					'address_2' => $patient->address_2,
					'city' => $patient->city,
					'state' => $patient->state,
					'postal_code' => $patient->postal_code,
					'country' => $patient->country
				]);
			?></td>
		</tr>
		<tr>
			<td width="30%">Test Name</td>
			<td><?php echo $patient->test_code.(! empty($patient->test_code) ? ' - ' : '').$patient->test_name; ?></td>
		</tr>
		<tr>
			<td width="30%">Lab Operator</td> 
			<td><?php echo $patient->staff_name_first.' '.$patient->staff_name_last; ?></td>
		</tr>
		<tr>
			<td width="30%">Lab Operator Location</td> 
			<td><?php echo $patient->staff_location; ?></td>
		</tr>
		<tr>
			<td width="30%">Lab Name</td>
			<td><?php echo $patient->lab_name.(! empty($patient->lab_location) ? ' ('.$patient->lab_location.')' : ''); ?></td>
		</tr>
		<tr>
			<td width="30%">Date Test Requested</td>
			<td><?php echo date('d-m-Y',strtotime($patient->date_test_result_request)); ?></td>
		</tr>
		<tr>
			<td width="30%">Date Test Service</td>
			<td><?php echo date('d-m-Y',strtotime($patient->date_test_result_service)); ?></td>
		</tr>
		<tr>
			<td width="30%">Diagnosis</td>
			<td><?php echo $patient->test_result_diagnosis; ?></td>
		</tr>
		</tbody>
		</table>
	</div>
	<br />
</div>
