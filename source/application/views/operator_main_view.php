<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
/*jslint browser: true*/
/*global $, jQuery, alert*/
"use strict"
$(function () {

	var cur_offset_patient = 0,
		cur_offset_test_result = 0,
		sort_field_patient = '',
		sort_by_patient = '',
		sort_field_test_result = '',
		sort_by_test_result = '',
		ID_patient_selected = 0
	
	$('[data-toggle="tooltip"]').tooltip()

	function get_ISO_Date(date_input_ui){
		var ret_date=''
		if(date_input_ui){
			var input_arr_1=date_input_ui.split("T")
			var input_2=input_arr_1[0]
			var input_arr_2=input_2.split(" ")
			var date_input=input_arr_2[0]
			if(date_input.indexOf('/') > 0){
				var date_parts=date_input.split("/")
				if(date_parts.length > 1){
					// date input mm/dd/yyyy
					var date_iso=new Date(date_parts[2] + '-' + date_parts[0] + '-' + date_parts[1])
					ret_date=date_iso.toISOString()
				}
			}
			else if(date_input.indexOf('-') > 0){
				var date_parts=date_input.split("-")
				if(date_parts.length > 1){
					if(parseInt(date_parts[0]) > 1900){
						// date input yyyy-mm-dd
						var date_iso=new Date(date_parts[0] + '-' + date_parts[1] + '-' + date_parts[2])
					}
					else{
						// date input mm-dd-yyyy
						var date_iso=new Date(date_parts[2] + '-' + date_parts[0] + '-' + date_parts[1])
					}
					ret_date=date_iso.toISOString()
				}
			}
			// NOTE : This function does not anticipate input of dd-mm-yyyy since its not possible to differentiate whether a value is a month or day 
		}
		return ret_date
	}

	/**
	 * Search for the patient listing to be displayed in a div, a filter field allows for the number of records to be limited. Includes pagination
	  *
	  * @param      none 
	  * @return string - HTML to insert into the page for the list of patients
	 */
	function build_patient_listing() {
		$.ajax({
			url: '<?php echo base_url("index.php/operator_con/ajax_patient_get_paged_list/") ?>' + cur_offset_patient,
			type: 'POST',
			data: {
				patient_filter: $('#id_patient_filter').val(),
				sort_field: sort_field_patient,
				sort_by: sort_by_patient
			},
			dataType: 'html',
			success: function (html_response) {
				$('div#id_patient_page_list').html(html_response)
			},
			error: function (http, status, error) {
				$('#id_page_response_message').text('Some unknown error occurred! build_patient_listing ' + error).addClass('text-danger').show()
			}
		})
	}
	
	/**
	 * Search for the test result listing to be displayed in a div for a single user. Includes pagination
	  *
	  * @param      none 
	  * @return string - HTML to insert into the page for the list of test results
	 */
	function build_test_result_listing() {
		$.ajax({
			url: '<?php echo base_url("index.php/operator_con/ajax_labtest_result_get_paged_list/") ?>' + cur_offset_test_result,
			type: 'POST',
			data: {
				FKID_patient: ID_patient_selected,
				test_result_filter: $('#id_test_result_filter').val(),
				sort_field: sort_field_test_result,
				sort_by: sort_by_test_result
			},
			dataType: 'html',
			success: function (html_response) {
				$('div#id_test_result_page_list').html(html_response)
				$(document.body).scrollTop($('#id_test_result_edit_form_container').offset().top)
			},
			error: function (http, status, error) {
				$('#id_page_response_message').text('Some unknown error occurred! build_test_result_listing ' + error).addClass('text-danger').show()
				$('#id_patient_test_results_container').hide()
			}
		})
	}
	
	/**
	 * Resort the patient listing, following the column heading that is clicked
	 */
	function set_sort_patient(sort_field) {
		cur_offset_patient = 0
		if (sort_field_patient.length > 0 && sort_field_patient.indexOf(sort_field) === 0 && sort_by_patient == 'asc') {
			sort_by_patient = 'desc'
		} 
		else {
			sort_by_patient = 'asc'
		}
		sort_field_patient = sort_field
		build_patient_listing()
	}
	
	/**
	 * Resort the test result listing, following the column heading that is clicked
	 */
	function set_sort_test_result(sort_field) {
		cur_offset_test_result = 0
		if (sort_field_test_result.length > 0 && sort_field_test_result.indexOf(sort_field) === 0 && sort_by_test_result == 'asc') {
			sort_by_test_result = 'desc'
		} 
		else {
			sort_by_test_result = 'asc'
		}
		sort_field_test_result = sort_field
		build_test_result_listing()
	}

	/**
	 * Prepare and display the Patient add or update form
	 */
	function showPatientDetailForm(){
		clear_patient_form()
		setDateInputs()
		$('#id_patient_edit_form').show()
		window.scrollTo(0, 0)
	}
	
	/**
	 * Specify settings and attach a calendar to a date field
	 */
	function setDateInputs(){
		var date = new Date();
		$('#id_patient_date_birth').datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: date.getFullYear()-100+':'+date.getFullYear(),
			showButtonPanel: true,
			dateFormat: 'mm/dd/yy',
			defaultDate: '-18y'
		})
	}

	// load data into the patient's details form
	function loadPatientDetailForm(ID_patient_click) {
		if ( ! ID_patient_click ) {
			$('#id_patient_response_message').text('Some unknown error occurred! There is no identifier.').addClass('text-danger').show()
		} 
		else if( ID_patient_selected == ID_patient_click ) {
			showPatientDetailForm()
			$('#id_ID_patient').val(ID_patient_selected)
			$('#id_patient_name_first').val($('#id_test_result_name_first').text())
			$('#id_patient_name_last').val($('#id_test_result_name_last').text())
			$('#id_patient_email').val($('#id_test_result_email_anchor').text())
			$('#id_patient_date_birth').val($('#id_test_result_date_birth').text())
			$('#id_patient_address_1').val($('#id_test_result_address_1').text())
			$('#id_patient_address_2').val($('#id_test_result_address_2').text())
			$('#id_patient_city').val($('#id_test_result_city').text())
			$('#id_patient_state').val($('#id_test_result_state').text())
			$('#id_patient_postal_code').val($('#id_test_result_postal_code').text())
			$('#id_patient_country').val($('#id_test_result_country').text())
			if($('#id_test_result_gender').text() == 'M') {
				$('#id_patient_gender_M').prop('checked', true)
			} 
			else if($('#id_test_result_gender').text() == 'F') {
				$('#id_patient_gender_F').prop('checked', true)
			}
			$('#id_patient_pass_code_display').text($('#id_test_result_patient_pass_code').text())
			$('#id_pass_code_block').show().css('display:inline')
			$('#id_patient_pass_code').val($('#id_test_result_patient_pass_code').text())
		}
		else if( ID_patient_click ) {
			ID_patient_selected = ID_patient_click
			$.getJSON({
				url: '<?php echo base_url("index.php/operator_con/ajax_patient_load/") ?>' + ID_patient_selected,
				success: function (json_obj) {
					showPatientDetailForm()
					// set form values
					$('#id_ID_patient').val(json_obj.ID_patient)
					$('#id_patient_name_first').val(json_obj.name_first)
					$('#id_patient_name_last').val(json_obj.name_last)
					$('#id_patient_email').val(json_obj.email)
					$('#id_patient_date_birth').val(json_obj.date_birth)
					$('#id_patient_address_1').val(json_obj.address_1)
					$('#id_patient_address_2').val(json_obj.address_2)
					$('#id_patient_city').val(json_obj.city)
					$('#id_patient_state').val(json_obj.state)
					$('#id_patient_postal_code').val(json_obj.postal_code)
					$('#id_patient_country').val(json_obj.country)
					if (json_obj.gender == 'M') {
						$('#id_patient_gender_M').prop('checked', true)
					} 
					else if (json_obj.gender == 'F') {
						$('#id_patient_gender_F').prop('checked', true)
					} 
					else {
						$('#id_patient_gender_M').prop('checked', false)
						$('#id_patient_gender_F').prop('checked', false)
					}
					$('#id_patient_pass_code_display').text(json_obj.patient_pass_code)
					$('#id_pass_code_block').show().css('display:inline')
					$('#id_patient_pass_code').val(json_obj.patient_pass_code)
				}
			})			
		}
		else{
			clear_patient_form()
			$('#id_patient_edit_form').hide()
			$('#id_pass_code_block').hide().css('display:inline')
			$('#id_page_response_message').text('Some unknown error occurred! Patient ID is missing!').addClass('text-danger').show()
		}
	}

	/**
	 * Delete a Patient, presenting a choice of whether to delete just the patient record, or all related test result pages as well
	 * @param integer The ID of the patient
	 */
	function delete_patient (ID_patient) {
		if (! ID_patient) {
			$('#id_page_response_message').text('Some unknown error occurred! There is no identifier.').addClass('text-danger').show()
		} 
		else {
			var dial_1 = swal({
					title: 'Are you sure that want to delete that patient?',
					type: 'warning',
					showCancelButton: true,
					confirmButtonText: 'Delete',
					closeOnConfirm: false
				},
				function (isConfirm) {
					if (isConfirm) {
						swal({
								title: 'Are you REALLY REALLY Sure?',
								type: 'warning',
								text: 'The Patient will be deleted, do you want to ALSO delete all test result records for that Patient?',
								showCancelButton: true,
								cancelButtonText: 'Delete Patient Only',
								confirmButtonText: 'Delete Patient & Test Results'
							},
							function (isConfirm) {
								if (isConfirm) { var flag = 'all' }
								else{ var flag = 'po' }
								$.get({
									url: '<?php echo base_url("index.php/operator_con/ajax_patient_delete/") ?>' + ID_patient,
									data: {w: flag},
									dataType: 'text',
									success: function (resp) {
										if ( resp == 1 ) {
											build_patient_listing()
											$('#id_page_response_message').text('Delete successful').addClass('text-success').show()
											if(ID_patient == ID_patient){
												ID_patient = 0
												$('#id_patient_test_results_container').hide()
												$('#id_patient_list_container').show()
											}
										} 
										else {
											$('#id_page_response_message').text('Delete failed').addClass('text-danger').show()
										}
									}
								})
								swal.close()
							}
						)
					}
				}
			)
		}
	}
	
	/**
	 * Clear out all values in the fields on the patient add / update form
	 */
	function clear_patient_form() {
		$('#id_ID_patient').val('')
		$('#id_patient_pass_code_display').text('')
		$('#id_patient_pass_code').val('')
		$('#id_pass_code_block').hide()
		$('#id_patient_name_first').val('')
		$('#id_patient_name_last').val('')
		$('#id_patient_email').val('')
		$('#id_patient_date_birth').val('')
		$('#id_patient_address_1').val('')
		$('#id_patient_address_2').val('')
		$('#id_patient_city').val('')
		$('#id_patient_state').val('')
		$('#id_patient_postal_code').val('')
		$('#id_patient_country').val('')
		$('#id_patient_gender_M').prop('checked', false)
		$('#id_patient_gender_F').prop('checked', false)
	}

	/**
	 * Prepare and display the test result add or update form
	 */
	function showTestResultDetailForm(){
		clear_test_result_form(false)
		setDateInputs_test_result()
		$('#id_patient_test_results_container').show()
		$('#id_test_result_edit_form_container').show()
		$('#id_patient_edit_form').hide()
		window.scrollTo(0, 0)
	}
		
	/**
	 * AJAX database lookup for the dropdown options used on the test result form
	 */
	function get_dropdown_options(w) {
		$.ajax({
			url: '<?php echo base_url("index.php/operator_con/ajax_get_option_json/") ?>',
			type: 'POST',
			data: {
				w: w
			},
			dataType: 'json',
			success: function (json_obj) {
				if (typeof json_obj == 'object') {
					var docfrag = document.createDocumentFragment()
					var i = 0
					$.each(json_obj, function (key, value) {
						var option = document.createElement('option')
						option.appendChild(document.createTextNode(value.name))
						option.setAttribute('id', 'id_' + w + '_' + i + '_option')
						option.setAttribute('value', value.id)
						docfrag.appendChild(option)
						i++
					})
					switch (w) {
						case 'test':
							document.getElementById('id_FKID_test_select').appendChild(docfrag)
							break;
						case 'lab':
							document.getElementById('id_FKID_lab_select').appendChild(docfrag)
							break;
						default:
							document.getElementById('id_FKID_staff_lab_select').appendChild(docfrag)
							break;
					}
				} 
				else {
					$('#id_page_response_message').text('Some unknown error occurred! get_dropdown_options- not object ' + error).addClass('text-danger').show()
				}
			},
			error: function (http, status, error) {
				$('#id_page_response_message').text('Some unknown error occurred! get_dropdown_options ' + error).addClass('text-danger').show()
			}
		})
	}
		
	/**
	 * Specify settings and attach a calendar to date fields on test result add / update form
	 */
	function setDateInputs_test_result(){
		var date = new Date();
		var arr_options={
			changeMonth: true,
			changeYear: true,
			yearRange: date.getFullYear()-20+':'+date.getFullYear(),
			showButtonPanel: true,
			dateFormat: 'mm/dd/yy',
			defaultDate: 0
		}
		$('#id_date_test_result_request').datepicker(arr_options)
		$('#id_date_test_result_service').datepicker(arr_options)
	}
	
	function reset_test_result_dropdowns(){
		$( '#id_FKID_test_select' ).select2({ 
			placeholder:'Select a Lab Test Name',
			allowClear: true,
			width:'100%'
		})
		$( '#id_FKID_staff_lab_select' ).select2({ 
			placeholder:'Select the Lab Staff Name',
			allowClear: true,
			width:'100%'
		})
		$( '#id_FKID_lab_select' ).select2({ 
			placeholder:'Select the Lab Location',
			allowClear: true,
			width:'100%'
		})
	}

	/**
	 * Clear out all values in the fields on the test result add / update form
	 */
	function clear_test_result_form(ishide) {	
		$('#id_test_result_email_button').hide()
		$('#id_test_result_pdf_button').hide()
		$('#id_ID_test_result').val('')
		$('#id_date_test_result_request').val('')
		$('#id_date_test_result_service').val('')
		$('#id_test_result_diagnosis').val('')
		$('#id_test_result_isEmailPatientSent').html('')
		$('#id_test_result_isEmailPatientSent').removeClass()
		$('#id_FKID_test_select').val('')
		$('#id_FKID_staff_lab_select').val('')
		$('#id_FKID_lab_select').val('')
		$('#id_FKID_test_select').trigger('change')
		$('#id_FKID_staff_lab_select').trigger('change')
		$('#id_FKID_lab_select').trigger('change')
		if(ishide == true) $('#id_test_result_edit_form_container').hide()
	}

	// *** Functions to run upon initialization
	// populate dropdown selectors on test result form with values from database
	get_dropdown_options('test')
	get_dropdown_options('lab')
	get_dropdown_options('staff')
	reset_test_result_dropdowns()
	// build list of patients upon initial page load
	build_patient_listing()

	$(document).click(function () {
		$('#id_page_response_message').text('').removeClass().hide()
		$('#id_patient_response_message').text('').removeClass().hide()
		$('#id_test_result_response_message').text('').removeClass().hide()
	})

	$('#id_logout').click(function () {
		window.location.replace('/index.php/login_operator_con/logout')
	})

	// ******************************
	// *** Functions to run as events due to user interaction
	// PATIENT SEARCH
	// rebuild list of patients using filter
	$('#id_patient_filter_butn').click(function (event) {
		event.preventDefault()
		cur_offset_patient = 0
		build_patient_listing()
	})

	$.each($('textarea[data-autoresize]'), function () {
		var offset = this.offsetHeight - this.clientHeight
		var resizeTextarea = function (el) {
			$(el).css('height', 'auto').css('height', el.scrollHeight + offset)
		}
		$(this).on('keyup input', function () {
			resizeTextarea(this)
		}).removeAttr('data-autoresize')
	})
	
	// redisplay the list of patients using the limiting criteria that was entered into the filter input
	$('#id_clear_filter_butn').click(function (event) {
		event.preventDefault()
		$('#id_patient_filter').val('')
	})

	// toggle for visibility of cur_page_patient_list_container_data
	$('#id_patient_list_container_data_toggle').click(function (event) {
		if ($('#id_patient_edit_form').is(':hidden')) {
			$('#id_patient_list_container').toggle()
		} 
		else {
			$('#id_patient_edit_form').hide()
		}
	})

	// set a new offset, then refresh the page for that offset of records
	$('div#id_patient_page_list').on('click', '[id^=id_page_link_]', function () {
		cur_offset_patient = $(this).attr('offset')
		build_patient_listing()
	})

	// display the edit (add/update) form patient form (floats over the operator main page)
	$('#id_patient_reveal_add_form_btn').click(function (event) {
		event.preventDefault()
		showPatientDetailForm()
	})

	// close the edit (add/update) patient form
	$('#id_patient_add_cancel1,#id_patient_add_cancel2').click(function (event) {
		event.preventDefault()
		clear_patient_form()
		$('#id_patient_edit_form').hide()
	})

	// clear the edit (add/update) patient form
	$('#id_patient_clear_butn').click(function (event) {
		event.preventDefault()
		clear_patient_form()
	})
	
	// clicking on column headings of the list in order to resort
	$('div#id_patient_page_list').on('click', 'div#id_name_last_sort', function () {
		set_sort_patient('name_last')
	})

	$('div#id_patient_page_list').on('click', 'div#id_city_sort', function () {
		set_sort_patient('city')
	})

	$('div#id_patient_page_list').on('click', 'div#id_state_sort', function () {
		set_sort_patient('state')
	})
	$('div#id_patient_page_list').on('click', 'div#id_gender_sort', function () {
		set_sort_patient('gender')
	})

	$('div#id_patient_page_list').on('click', 'div#id_date_birth_sort', function () {
		set_sort_patient('date_birth')
	})

	$('div#id_patient_page_list').on('click', '[data-toggle=tooltip]', function (event) {
		event.preventDefault()
		$(this).tooltip({delay: {show: 0, hide: 1500}})
	})

	// make request to load data from patient listing
	$('div#id_patient_page_list').on('click', '[id^=id_load_]', function (event) {
		event.preventDefault()
		loadPatientDetailForm($(this).attr('id_patient'))
	}) 

	// make request to load data from patient details on test result panel
	$('#id_display_patient_details_update').click(function (event) {
		event.preventDefault()
		loadPatientDetailForm(ID_patient_selected)
	})

	// view patient's details
	$('div#id_patient_page_list').on('click', '[id^=id_view_]', function (event) {
		event.preventDefault()
		var ID_patient = $(this).attr('id_patient')
		if (!ID_patient) {
			$('#id_patient_response_message').text('Some unknown error occurred! There is no identifier.').addClass('text-danger').show()
		} 
		else{
			$('#id_display_test_result_name').html('')
			// ajax to get patient details and test results and generate HTML that goes below
			$.getJSON({
				url: '<?php echo base_url("index.php/operator_con/ajax_patient_load/") ?>' + ID_patient,
				success: function (json_obj) {
					// set the js page variable to the id of the selected patient
					ID_patient_selected = ID_patient
					// set form values
					$('#id_test_result_name_first').text(json_obj.name_first)
					$('#id_test_result_name_last').text(json_obj.name_last)
					$('#id_test_result_email').html('<a id="id_test_result_email_anchor" href="mailto:' + json_obj.email + '">' + json_obj.email + '</a>')
					$('#id_test_result_date_birth').text(json_obj.date_birth)
					$('#id_test_result_gender').text(json_obj.gender)
					$('#id_test_result_address_1').text(json_obj.address_1)
					$('#id_test_result_address_2').text(json_obj.address_2)
					$('#id_test_result_city').text(json_obj.city)
					$('#id_test_result_state').text(json_obj.state)
					$('#id_test_result_postal_code').text(json_obj.postal_code)
					$('#id_test_result_country').text(json_obj.country)
					$('#id_test_result_patient_pass_code').text(json_obj.patient_pass_code)
					$('#id_patient_address_full').html(json_obj.patient_address_full)
					// set the name of the patient in the panel heading
					$('#id_title_patient_name').text(json_obj.name_first + ' ' + json_obj.name_last)
					// set visibility on page elements
					$('#id_patient_list_container').hide()
					$('#id_patient_edit_form').hide()
					$('#id_patient_test_results_container').show()
					build_test_result_listing()
				}
			})
		}
	})
	
	// delete a patient - click from the patient listing
	$('div#id_patient_page_list').on('click', '[id^=id_delete_]', function (event) {
		event.preventDefault()
		var ID_patient = $(this).attr('id_patient')
		if( ID_patient ){ delete_patient(ID_patient) }
	})

	// delete a patient - click from Patient Test results panel
	$('#id_display_patient_details_delete').click(function (event) {
		event.preventDefault()
		if( ID_patient_selected ){ delete_patient(ID_patient_selected) }
	})

	// submit the patient form - this can be for an add or update
	$('#id_patient_submit_butn').click(function (event) {
		event.preventDefault()
		var ID_patient = $('#id_ID_patient').val()
		if( ! ID_patient ){ ID_patient = 0 }
		var data_array = {
			'ID_patient': ID_patient,
			'patient_pass_code': $('#id_patient_pass_code').val(),
			'name_first': $('#id_patient_name_first').val(),
			'name_last': $('#id_patient_name_last').val(),
			'email': $('#id_patient_email').val(),
			'address_1': $('#id_patient_address_1').val(),
			'address_2': $('#id_patient_address_2').val(),
			'city': $('#id_patient_city').val(),
			'state': $('#id_patient_state').val(),
			'postal_code': $('#id_patient_postal_code').val(),
			'country': $('#id_patient_country').val(),
			'gender': $('input[name="gender"]:checked').val(),
			'date_birth': get_ISO_Date($('#id_patient_date_birth').val())
		}
		$.ajax({
			url: '<?php echo base_url("index.php/operator_con/ajax_patient_save_process/") ?>',
			type: 'POST',
			data: {
				post_json: data_array
			},
			dataType: 'json',
			success: function (json_obj) {
				if (json_obj.result == 1) {
					$('#id_patient_response_message').text('Error, your entries were not saved. ' + json_obj.result_message).addClass('text-danger').show()
				} 
				else if (json_obj.result == 2) {
					$('#id_patient_response_message').text(json_obj.result_message).addClass('text-success').show()
					$('#id_ID_patient').val(json_obj.ID_patient)
					$('#id_patient_pass_code_display').text(json_obj.patient_pass_code)
					$('#id_patient_pass_code').val(json_obj.patient_pass_code)
					$('#id_pass_code_block').show().css('display:inline')
				}
				build_patient_listing()
				window.scrollTo(0, 0)
			},
			error: function (http, status, error) {
				$('#id_patient_response_message').text('Some unknown error occurred! id_patient_submit_butn ' + error).addClass('text-danger').show()
			}
		})
	})

	// ******************************
	// TEST RESULTS (FOR A SINGLE PATIENT)
	// rebuild list of test results using filter
	$('#id_test_result_filter_butn').click(function (event) {
		event.preventDefault()
		cur_offset_test_result = 0
		build_test_result_listing()
	})

	$('#id_test_result_filter').change(function () {
		build_test_result_listing()
	})

	// toggle for visibility of test_result_list
	$('#id_test_result_list_container_data_toggle').click(function (event) {
		$('#id_test_result_edit_form_container').hide()
		$('#id_test_result_list').toggle()
	})

	// set a new offset, then refresh the page for that offset of records
	$('div#id_test_result_page_list').on('click', '[id^=id_page_link_]', function () {
		cur_offset_test_result = $(this).attr('offset')
		build_test_result_listing()
	})

	// display the edit (add/update) form test result form (floats over the operator main page)
	$('#id_test_result_reveal_add_form_btn').click(function (event) {
		event.preventDefault()
		showTestResultDetailForm()
	})

	// close the edit (add/update) test result form
	$('#id_test_result_add_cancel1,#id_test_result_add_cancel2').click(function (event) {
		event.preventDefault()
		clear_test_result_form(true)
		$('#id_test_result_edit_form_container').hide()
	})

	// clicking on column headings of the list in order to resort
	$('div#id_test_result_page_list').on('click', 'div#id_date_test_result_request_sort', function () {
		set_sort_test_result('tr.date_test_result_request')
	})

	$('div#id_test_result_page_list').on('click', 'div#id_date_test_result_service_sort', function () {
		set_sort_test_result('tr.date_test_result_service')
	})

	$('div#id_test_result_page_list').on('click', 'div#id_tr_test_name_sort', function () {
		set_sort_test_result('t.test_name')
	})

	$('div#id_test_result_page_list').on('click', 'div#id_tr_lab_staff_name_sort', function () {
		set_sort_test_result('s.lab_staff_name')
	})

	$('div#id_test_result_page_list').on('click', 'div#id_tr_lab_name_sort', function () {
		set_sort_test_result('l.lab_name')
	})

	// LOAD data into the edit (update) test result form
	$('div#id_test_result_page_list').on('click', '[id^=id_load_]', function (event) {
		event.preventDefault()
		var ID_test_result = $(this).attr('id_test_result')
		if (!ID_test_result) {
			$('#id_test_result_response_message').text('Some unknown error occurred! There is no identifier.').addClass('text-danger').show()
		} 
		else {
			$.ajax({
				url: '<?php echo base_url("index.php/operator_con/ajax_labtest_result_load/") ?>' + ID_test_result,
				method: 'POST',
				dataType: 'json',
				success: function (json_obj) {
					// clears table of any existing data
					showTestResultDetailForm()
					// set values from test result table
					$('#id_display_test_result_name').html($('#id_title_patient_name').html())
					$('#id_ID_test_result').val(json_obj.ID_test_result)
					$('#id_date_test_result_request').val(json_obj.date_test_result_request)
					$('#id_date_test_result_service').val(json_obj.date_test_result_service)
					$('#id_test_result_diagnosis').val(json_obj.test_result_diagnosis)
					if(json_obj.isEmailPatientSent == '1'){
						$('#id_test_result_isEmailPatientSent').html('Send Completed')
						$('#id_test_result_isEmailPatientSent').addClass('text-info')
					}
					else{
						$('#id_test_result_isEmailPatientSent').html('Not Sent')
						$('#id_test_result_isEmailPatientSent').addClass('text-warning')
					}
					$('#id_FKID_test_select').val(json_obj.FKID_test)
					$('#id_FKID_staff_lab_select').val(json_obj.FKID_staff_lab)
					$('#id_FKID_lab_select').val(json_obj.FKID_lab)
					$('#id_FKID_test_select').trigger('change')
					$('#id_FKID_staff_lab_select').trigger('change')
					$('#id_FKID_lab_select').trigger('change')
					$('#id_test_result_email_button').show()
					$('#id_test_result_pdf_button').show()
				}
			})
		}
	})

	// view test_result's details
	$('div#id_test_result_page_list').on('click', '[id^=id_view_]', function (event) {
		event.preventDefault()
		var ID_test_result = $(this).attr('id_test_result')
		if( ! ID_test_result ) {
			$('#id_page_response_message').text('Some unknown error occurred! There is no identifier.').addClass('text-danger').show()
		} 
		else {
			window.open('<?php echo base_url("index.php/operator_con/patient_test_result_report/")."/" ?>' + ID_test_result)
			return false
		}
	})

	// delete a test result
	$('div#id_test_result_page_list').on('click', '[id^=id_delete_]', function (event) {
		event.preventDefault()
		var ID_test_result = $(this).attr('id_test_result')
		if( ! ID_test_result ) {
			$('#id_test_result_response_message').text('Some unknown error occurred! There is no identifier.').addClass('text-danger').show()
		} 
		else {
			var confirm_res = swal({
					title: "Are you sure that want to delete that test result?",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: "Delete"
				},
				function (isConfirm) {
					if (isConfirm) {
						$.getJSON({
							url: '<?php echo base_url("index.php/operator_con/ajax_labtest_result_delete/") ?>' + ID_test_result,
							success: function (resp) {
								if ( resp == 1 ) {
									build_test_result_listing()
									$('#id_page_response_message').text('Delete successful').addClass('text-success').show()
								} 
								else {
									$('#id_page_response_message').text('Delete failed').addClass('text-danger').show()
								}
							}
						})
					}
				}
			)
		}
	})
	// submit the test result form - this can be for an add or update
	$('#id_test_result_submit_butn').click(function (event) {
		event.preventDefault()
		var ID_test_result = $('#id_ID_test_result').val()
		var data_array = {
			'ID_test_result':ID_test_result,
			'FKID_patient':ID_patient_selected,
			'FKID_test': $('#id_FKID_test_select').val(),
			'FKID_staff_lab': $('#id_FKID_staff_lab_select').val(),
			'FKID_lab': $('#id_FKID_lab_select').val(),
			'date_test_result_request': get_ISO_Date($('#id_date_test_result_request').val()),
			'date_test_result_service': get_ISO_Date($('#id_date_test_result_service').val()),
			'test_result_diagnosis': $('#id_test_result_diagnosis').val()
		}
		$.ajax({
			url: '<?php echo base_url("index.php/operator_con/ajax_labtest_result_save_process/") ?>',
			type: 'POST',
			data: {
				post_json: data_array
			},
			dataType: 'json',
			success: function (json_obj) {
				if (json_obj.result == 'fail') {
					$('#id_test_result_response_message').text('Error, your entries were not saved. ' + json_obj.result_message).addClass('text-danger').show()
				} 
				else if((json_obj.result == 'add') || (json_obj.result == 'update')) {
					$('#id_test_result_response_message').text(json_obj.result_message).addClass('text-success').show()
					$('#id_ID_test_result').val(json_obj.ID_test_result)
					$('#id_test_result_email_button').show()
					$('#id_test_result_pdf_button').show()
				}
				build_test_result_listing()
				window.scrollTo(0, 0)
			},
			error: function (http, status, error) {
				$('#id_test_result_response_message').text('Some unknown error occurred! id_test_result_submit_butn ' + error).addClass('text-danger').show()
			}
		})
	})

	// submit test_result pdf button
	$('div#id_test_result_edit_form_container').on('click', '#id_test_result_pdf_button', function (event) {
		event.preventDefault()
		var ID_test_result = $('#id_ID_test_result').val()
		var patient_pass_code = $('#id_test_result_patient_pass_code').text()
		window.open("<?php echo site_url('patient_con/pdf/') ?>"+ID_test_result+"/"+patient_pass_code, "_blank", "toolbar=no,width=600,height=600,scrollbars=yes")
	})

	// submit test_result email button
	$('div#id_test_result_edit_form_container').on('click', '#id_test_result_email_button', function (event) {
		event.preventDefault()
		var ID_test_result = $('#id_ID_test_result').val()
		if( ! ID_test_result ) {
			clear_test_result_form(true)
			$('#id_test_result_edit_form_container').hide()
			$('#id_page_response_message').text('Some unknown error occurred! There is no identifier. id_test_result_email_button').addClass('text-danger').show()
		} 
		else {
			$.ajax({
				url: '<?php echo base_url("index.php/operator_con/ajax_send_email/") ?>',
				type: 'POST',
				data: {
					ID_test_result:ID_test_result,
					patient_pass_code: $('#id_test_result_patient_pass_code').text(),
					email_pdf: $('#id_test_result_email').text()
				},
				dataType: 'text',
				success: function (res) {
					$('#id_test_result_isEmailPatientSent').removeClass()
					if(res == 'Email Failed'){
						$('#id_test_result_response_message').text(res).addClass('text-danger').show()
						$('#id_test_result_isEmailPatientSent').html('Email Failed')
						$('#id_test_result_isEmailPatientSent').addClass('text-danger')
					}
					else{
						$('#id_test_result_response_message').text(res).addClass('text-success').show()
						$('#id_test_result_isEmailPatientSent').html('Send Completed')
						$('#id_test_result_isEmailPatientSent').addClass('text-success')
						$('#id_'+ID_test_result+'test_result_isEmailPatientSent').html('Yes')
					}
				},
				error: function (http, status, error) {
					$('#id_test_result_response_message').text('Some unknown error occurred! id_test_result_email_button ' + res + ' ' + error).addClass('text-danger').show()
				}
			})
		}
	})

})
</script>
<div class="container">
	<div class="row headerbg">
		<div class="col-md-2">
			<img src="/assets/images/axis_logo_tbg.png" alt="logo" width="96" height="52" />
		</div>
		<div class="col-md-10">
			<h3>Axis Labs - Pathology Lab Reporting</h3>
		</div>
	</div><!-- end row -->
	<div class="row" style="padding-top:5px;">
		<div class="col-md-10">			
			<span class="lead">Lab Operator Main</span>
		</div>
		<div class="col-md-2">
			<?php
				if($this->session->has_userdata('displayname')){
					echo $_SESSION['displayname'];
				}
				echo form_button([
					'id' => 'id_logout',
					'name' => 'logout',
					'content' => 'Logout',
					'class' => 'btn btn-primary btn-sm',
					'style' => ';margin-left:15px;'
				]);
			?>
			</p>
		</div><!-- end col -->
	</div><!-- end row -->
	<div class="row">
		<div class="col-md-12">
			<h2 id="id_page_response_message" style="display:none;"  class=""></h2>
			<div class="panel-group">
				<div id="id_patient_edit_form" style="display:none;" class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						<div style="top:-100px;outline-style:solid;outline-color:black;position:absolute;background-color:white;z-index:9;" class="panel panel-primary">
							<div class="panel-heading"><h3 class="panel-title">Patient Details</h3> <a id="id_patient_add_cancel1" style="top:3px;color:white;position:absolute;right:20px;">&lt;&lt;&nbsp;Close&nbsp;&gt;&gt;</a></div>
							<div class="panel-body">
								<h2 id="id_patient_response_message" style="display:none;" class=""></h2>
								<?php
									echo form_open('#',['id'=>'id_form_patient_edit','class'=>'form-horizontal','method'=>'post','role'=>'form']);
										echo form_input(['type'=>'hidden','name'=>'ID_patient','id'=>'id_ID_patient']);
										echo form_input(['type'=>'hidden','name'=>'patient_pass_code','id'=>'id_patient_pass_code']);
										echo '<div class="form-group row">'."\n";
											echo form_label('First&nbsp;Name&nbsp;','name_first',['class' => 'col-md-3 control-label']);
											echo '<div class="col-md-3">';
												echo form_input('name_first','',['id' => 'id_patient_name_first', 'class' => 'form-control input-group-lg']);
											echo "</div>\n";
											echo form_label('Last&nbsp;Name&nbsp;','name_last',['class' => 'col-md-2 control-label']);
											echo '<div class="col-md-4">';
												echo form_input('name_last','',['id' => 'id_patient_name_last', 'class' => 'form-control input-group-lg']);
											echo "</div>\n";
										echo "</div>\n";

										echo '<div class="form-group row">'."\n";
											echo form_label('Email&nbsp;','email',['class' => 'col-md-3 control-label']);
											echo '<div class="col-md-6">';
												echo form_input('email','',['id' => 'id_patient_email', 'class' => 'form-control input-group-lg']);
											echo "</div>\n";
										echo "</div>\n";
			
										echo '<div class="form-group row">'."\n";
											echo form_label('Date&nbsp;of&nbsp;Birth&nbsp;','date_birth',['class' => 'col-md-3 control-label']);
											echo '<div class="col-md-6">';
												echo form_input('date_birth','',['id' => 'id_patient_date_birth', 'class' => 'form-control input-group-lg']);
											echo "</div>\n";
										echo "</div>\n";

										echo '<div class="form-group row">'."\n";
											echo form_label('Gender&nbsp;','gender_M',['class' => 'col-md-3 control-label']);
											echo '<div class="col-md-6">';
												echo '<div class="radio-inline"><label>';
													echo form_radio('gender', 'M', false, 'id=id_patient_gender_M');
												echo "M</label></div>\n";
												echo '<div class="radio-inline"><label>';
													echo form_radio('gender', 'F', false, 'id=id_patient_gender_F');
												echo "F</label></div>\n";
											echo "</div>\n";
										echo "</div>\n";

										echo '<div class="form-group row">'."\n";
											echo form_label('Address&nbsp;1&nbsp;','address1',['class' => 'col-md-3 control-label']);
											echo '<div class="col-md-6">';
												echo form_input('address_1','',['id' => 'id_patient_address_1', 'class' => 'form-control input-group-lg']);
											echo "</div>\n";
										echo "</div>\n";

										echo '<div class="form-group row">'."\n";
											echo form_label('Address&nbsp;2&nbsp;','address2',['class' => 'col-md-3 control-label']);
											echo '<div class="col-md-6">';
												echo form_input('address_2','',['id' => 'id_patient_address_2', 'class' => 'form-control input-group-lg']);
											echo "</div>\n";
										echo "</div>\n";

										echo '<div class="form-group row">'."\n";
											echo form_label('City&nbsp;','city',['class' => 'col-md-3 control-label']);
											echo '<div class="col-md-6">';
												echo form_input('city','',['id' => 'id_patient_city', 'class' => 'form-control input-group-lg']);
											echo "</div>\n";
										echo "</div>\n";

										echo '<div class="form-group row">'."\n";
											echo form_label('State&nbsp;','state',['class' => 'col-md-3 control-label']);
											echo '<div class="col-md-6">';
												echo form_input('state','',['id' => 'id_patient_state', 'class' => 'form-control input-group-lg']);
											echo "</div>\n";
										echo "</div>\n";

										echo '<div class="form-group row">'."\n";
											echo form_label('Postal&nbsp;Code&nbsp;','postal_code',['class' => 'col-md-3 control-label']);
											echo '<div class="col-md-6">';
												echo form_input('postal_code','',['id' => 'id_patient_postal_code', 'class' => 'form-control input-group-lg']);
											echo "</div>\n";
										echo "</div>\n";

										echo '<div class="form-group row">'."\n";
											echo form_label('Country&nbsp;','country',['class' => 'col-md-3 control-label']);
											echo '<div class="col-md-6">';
												echo form_input('country','',['id' => 'id_patient_country', 'class' => 'form-control input-group-lg']);
											echo "</div>\n";
										echo "</div>\n";
										
										echo '<div id="id_pass_code_block" style="display:none;">'."\n";
											echo '<div id="form-group row">'."\n";
												echo form_label('Pass&nbsp;Code&nbsp;','',['class' => 'col-md-3 control-label']);
												echo '<div class="col-md-6">';
													echo '<p class="form-control-static"><span id="id_patient_pass_code_display"></span></p>';
												echo "</div>\n";
											echo "</div>\n";
										echo "</div>\n";
										
										echo '<div class="form-group row">'."\n";
											echo '<div class="col-md-6"></div>';
											echo '<div class="col-md-4">';
												echo form_button('patient_submit_butn','Submit Patient Details',['id' => 'id_patient_submit_butn', 'class' => 'btn btn-default']);
											echo "</div>\n";
										echo "</div>\n";
										echo '<div class="row">'."\n";
											echo '<div class="col-md-6"></div>';
											echo '<div class="col-md-2">';
												echo form_button('patient_close_butn','Close',['id' => 'id_patient_add_cancel2', 'class' => 'btn btn-default']);
											echo "</div>\n";
											echo '<div class="col-md-2">';
												echo form_button('patient_clear_butn','Clear Form',['id' => 'id_patient_clear_butn', 'class' => 'btn btn-default']);
											echo "</div>\n";
										echo "</div>\n";
									echo form_close();
								?>
							</div><!-- end panel-body -->
						</div><!-- end panel panel panel-primary -->
					</div><!-- end col -->
				</div><!-- end row id_patient_edit_form -->
				<div id="id_search_patient_panel" class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-md-2">
								<h1 class="panel-title">Search Patients </h1>
							</div>
							<div class="col-md-9">
								<?php echo form_button('display_add_patient','Add New Patient',['class'=>'btn btn-default','id'=>'id_patient_reveal_add_form_btn','style'=>'float:right;']); ?>
							</div>
							<div class="col-md-1">
								<a id="id_patient_list_container_data_toggle" href="#id_patient_list_container" style="float:right;color:white;text-decoration:underline;">Listing </a>
							</div>
						</div>
					</div>
					<div id="id_patient_list_container" class="panel-body">
						<div class="row">
							<div class="col-md-6"></div>
							<div class="col-md-6">
								<?php 
									echo form_open('#',	['id'=>'id_form_patient_search','class'=>'form-inline','method'=>'post','role'=>'form']);
										echo '<div class="form-group">'."\n";
											echo form_label('Patients search terms&nbsp;','patient_filter_label',['class' => 'control-label']);
											echo form_input(['class'=>'form-control','id'=>'id_patient_filter','maxlength'=>'100','name'=>'patient_filter','placeholder'=>'Patients search terms','size'=>'50','style'=>'width:50%','type'=>'text']);
											echo form_button('patient_filter_butn','Filter',['class' => 'btn btn-default','id' => 'id_patient_filter_butn']);
											echo form_button('clear_filter_butn','X',['class' => 'btn btn-default','id' => 'id_clear_filter_butn']);
										echo "</div>\n";
									echo form_close();
								?>
							</div>
						</div><!-- end row -->
						<div class="row">
							<div id="id_patient_page_list" class="col-md-12"></div>
						</div>
					</div><!-- end id_patient_list_container -->
				</div><!-- end panel id_search_patient_panel -->
				<!-- Test Results - - hidden form to add and update test results  -->
				<div id="id_test_result_edit_form_container" style="display:none;outline-style:solid;outline-color:black;z-index:9;top:-100px;width:75%;position:absolute;left:10%;">
					<div class="panel panel-primary">
						<div class="panel-heading"><h3 class="panel-title">Test Results</h3> <a id="id_test_result_add_cancel1" style="top:3px;color:white;position:absolute;right:20px;">&lt;&lt;&nbsp;Close&nbsp;&gt;&gt;</a></div>
						<div style="background-color:white;" class="panel-body">
							<h2 id="id_test_result_response_message" style="display:none;"></h2>
							<h4><span class="smallgrey">Patient Name: </span><span id="id_display_test_result_name"></span>
							</h4>
							<?php
								echo form_open('#',['id'=>'id_test_result_edit_form','method'=>'post','role'=>'form']);
								echo form_input(['type'=>'hidden','name'=>'test_result','id'=>'id_ID_test_result']);
								echo '<div class="row">'."\n";
									echo '<div class="col-md-12">'."\n";
										echo '<div class="form-group">'."\n";
											echo form_label('Lab Test&nbsp;','FKID_test',['class' => 'control-label']);
											echo '<select id="id_FKID_test_select" name="FKID_test" class="form-control"><option></option></select>';
										echo "</div>\n";
									echo "</div>\n";
								echo "</div>\n";
								echo '<div class="row">'."\n";
									echo '<div class="col-md-6">'."\n";
										echo '<div class="form-group">'."\n";
											echo form_label('Lab Staff&nbsp;','FKID_staff_lab',['class' => 'control-label']);
											echo '<select id="id_FKID_staff_lab_select" name="FKID_staff_lab" class="form-control"><option></option></select>';
										echo "</div>\n";
									echo "</div>\n";
									echo '<div class="col-md-6">'."\n";
										echo '<div class="form-group">'."\n";
											echo form_label('Lab Name&nbsp;','FKID_staff_lab',['class' => 'control-label']);
											echo '<select id="id_FKID_lab_select" name="FKID_lab" class="form-control"><option></option></select>';
										echo "</div>\n";
									echo "</div>\n";
								echo "</div>\n";
								echo '<div class="row">'."\n";
									echo '<div class="col-md-6">'."\n";
										echo '<div class="form-group">'."\n";
											echo form_label('Date&nbsp;Request&nbsp;','date_test_result_request',['class' => 'col-md-3 control-label']);
											echo form_input('Date_test_result_request','',['id' => 'id_date_test_result_request', 'class' => 'form-control']);
										echo "</div>\n";
									echo "</div>\n";
									echo '<div class="col-md-6">'."\n";
										echo '<div class="form-group">'."\n";
											echo form_label('Date&nbsp;Service&nbsp;','date_test_result_service',['class' => 'col-md-3 control-label']);
											echo form_input('date_test_result_service','',['id' => 'id_date_test_result_service', 'class' => 'form-control']);
										echo "</div>\n";
									echo "</div>\n";
								echo "</div>\n";
								echo '<div class="form-group">'."\n";
									echo form_label('Diagnosis&nbsp;','test_result_diagnosis',['class' => 'col-md-3 control-label']);
									echo '<textarea id="id_test_result_diagnosis" name="test_result_diagnosis" cols="40" rows="2" data-autoresize style="resize:none;" class="form-control"></textarea>';
								echo "</div>\n";
								echo '<div class="form-group">'."\n";
									echo '<div class="col-md-4">';
										echo '<button id="id_test_result_pdf_button" style="display:none;" class="btn btn-default">Open PDF</button>';
									echo "</div>\n";
									echo '<div class="col-md-4">';
										echo '<span id="id_test_result_isEmailPatientSent"></span>&nbsp;&nbsp;&nbsp;';
										echo '<button id="id_test_result_email_button" style="display:none;" class="btn btn-default">Send Email to the Patient</button>';
									echo "</div>\n";
									echo '<div class="col-md-4">';
										echo form_button('test_result_submit_butn','Submit Test Result',['id' => 'id_test_result_submit_butn', 'class' => 'btn btn-default']);
									echo "</div>\n";
								echo "</div>\n";
								echo '<div class="col-md-7"></div>';
								echo '<div class="col-md-4">';
									echo '<a id="id_test_result_add_cancel2">&lt;&lt;&nbsp;Close&nbsp;&gt;&gt;</a>';
								echo "</div>\n";
								echo form_close();
							?>
						</div><!-- end panel-body -->
					</div><!-- end panel -->
				</div><!-- end id_test_result_edit_form_container -->
				<div id="id_patient_test_results_container" style="display:none;padding-top:15px;">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<div class="row">
								<div class="col-md-12">
									<h2 class="panel-title">Patient Details for <span id="id_title_patient_name"></span></h2>
								</div>
							</div>
						</div>
						<div id="id_cur_patient_record" class="panel-body">
							<div class="row">
								<div class="col-md-1"><!-- blank --></div>
								<div class="col-md-5">
									<div class="row">
										<div class="col-md-4 smallgrey">Name First</div>
										<div class="col-md-6"><span id="id_test_result_name_first"></span></div>
									</div>
									<div class="row">
										<div class="col-md-4 smallgrey">Name Last</div>
										<div class="col-md-6"><span id="id_test_result_name_last"></span></div>
									</div>
									<div class="row">
										<div class="col-md-4 smallgrey">Email</div>
										<div class="col-md-6"><span id="id_test_result_email"></span></div>
									</div>
									<div class="row">
										<div class="col-md-4 smallgrey">Birth Date</div>
										<div class="col-md-6"><span id="id_test_result_date_birth"></span></div>
									</div>
									<div class="row">
										<div class="col-md-4 smallgrey">Gender</div>
										<div class="col-md-6"><span id="id_test_result_gender"></span></div>
									</div>
									<div class="row">
										<div class="col-md-4 smallgrey">Pass Code</div>
										<div class="col-md-6"><span id="id_test_result_patient_pass_code"></span></div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="smallgrey">Address</div>
									<span id="id_patient_address_full"></span>
									<br>
									<br>
									<span id="id_test_result_address_1" style="display:none;"></span>
									<span id="id_test_result_address_2" style="display:none;"></span>
									<span id="id_test_result_city" style="display:none;"></span>
									<span id="id_test_result_state" style="display:none;"></span>
									<span id="id_test_result_postal_code" style="display:none;"></span>
									<span id="id_test_result_country" style="display:none;"></span>
								</div>
								<div class="col-md-2">
									<p id="id_display_patient_details_update" style="text-decoration:underline;">Update Patient Details</p>
									<br>
									<p id="id_display_patient_details_delete" style="text-decoration:underline;">Delete Patient</p>
								</div>
							</div>
							<div id="id_test_result_container" style="margin-top:15px;" class="panel panel-info">
								<div class="panel-heading">
									<div class="row">
										<div class="col-md-2">
											<h2 class="panel-title">Test Results </h2>
										</div>
										<div class="col-md-8">
											<?php echo form_button('display_add_test_result_btn','Add New Test Result',['id'=>'id_test_result_reveal_add_form_btn','class'=>'btn btn-default','style'=>'float:right;']); ?>
										</div>
										<div class="col-md-2">
											<a id="id_test_result_list_container_data_toggle" href="#id_test_result_list" style="float:right;color:black;text-decoration:underline;">Listing </a>
										</div>
									</div>
								</div>
								<div id="id_test_result_list" class="panel-body">
									<div class="row">
										<div class="col-md-6"></div>
										<div class="col-md-6" style="float:right;padding-top:10px;">
											<?php 
												echo form_open('#',	['id'=>'id_form_test_result_filter','class'=>'form-inline','method'=>'post','role'=>'form']);
													echo '<div class="form-group">'."\n";
														echo form_label('Filter&nbsp;Test&nbsp;Results&nbsp;','test_result_filter_label',['class' => 'control-label']);
														echo form_dropdown('test_result_filter',['999' => 'all', '1' =>	'previous month','6' => 'previous six months','12' => 'previous year'],'6',['class'=>'form-control','id'=>'id_test_result_filter']);
														echo form_button('filter_test_result_butn','Filter',['id' => 'id_test_result_filter_butn','class' => 'btn btn-default']);
													echo "</div>\n";
												echo form_close();
											?>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div id="id_test_result_page_list" class="col-md-12"></div>
										</div>
									</div><!-- end row -->
								</div><!-- end id_test_result_container -->
							</div>
						</div>
					</div><!-- end panel panel-primary -->
				</div><!-- end panel id_patient_test_results_container -->
			</div><!-- end panel-group -->
		</div><!-- end col -->
	</div><!-- end row -->
</div><!-- end container -->
