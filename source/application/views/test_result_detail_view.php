<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
$(function() {

	$('#id_btn_pdf').click(function(event){
		event.preventDefault()
		window.open("<?php echo site_url('patient_con/pdf/'.$ID_test_result.'/'.$patient_pass_code) ?>", "_blank", "toolbar=no,width=600,height=600,scrollbars=yes")
	})

	$('#id_btn_return').click(function(event){
		event.preventDefault()
		$(location).attr('href',"<?php echo base_url('index.php/patient_con/get_results/'.$patient_pass_code) ?>")
	})

})
</script>
<div class="container">
	<div class="row headerbg">
		<div class="col-md-2">
			<img src="/assets/images/axis_logo_tbg.png" alt="logo" width="96" height="52" />
		</div>
		<div class="col-md-10">
			<h3>Axis Labs - Pathology Lab Reporting</h3>
		</div>
	</div>
	<div class="row" style="padding-top:20px;padding-left:15px;">
		<div class="lead">Lab Test Results for <?php echo $name_first.' '.$name_last ?></div>
		<?php 
			if($this->session->flashdata('errors')) {
				echo '<div class="text-danger">'.$this->session->flashdata('errors').'</div>';
			}
		?>
	</div>
	<div class="well" style="padding-top:0;">
		<div class="row">
			<div class="col-md-6">
				<h4><span class="label label-default">Test Results</span></h4>
				<table class="table table-bordered table-condensed"><tbody>
				<tr>
					<td>Test Performed</td> 
					<td><?php echo $test_code.(! empty($test_code) ? ' - ' : '').$test_name ?></td>
				</tr>
				<tr>
					<td>Date Test Requested</td> 
					<td width="80%"><?php echo date('d-m-Y',strtotime($date_test_result_request)) ?></td>
				</tr>
				<tr>
					<td>Date Test Completed</td> 
					<td><?php echo date('d-m-Y',strtotime($date_test_result_service)) ?></td>
				</tr>
				</tbody></table>
			</div>
			<div class="col-md-6">
				<div class="panel panel-default" style="margin-top:10px;">
					<div class="panel-body">
					  <?php 
						echo "<p>";
						echo form_button('btn_return','Return to List',['id' => 'id_btn_return', 'class' => 'btn btn-primary']);
						echo "</p><p>";
						echo form_button('btn_pdf','View PDF',['id' => 'id_btn_pdf', 'class' => 'btn btn-primary', 'style' => 'padding-top:10px']);
						echo "</p>";
						echo form_open('patient_con/email/', ['class' => 'form-inline','method' => 'post','role' => 'form']);
							echo form_hidden('ID_test_result',$ID_test_result);
							echo form_hidden('patient_pass_code',$patient_pass_code);
							if($this->session->flashdata('errors_email')) {
								foreach($this->session->flashdata('errors_email') AS $value)
								echo '<p class="text-danger">'.$value.'</p>';
							}
							echo '<div class="form-group">'."\n";
								echo form_submit('email_btn', 'Send Email',['id' => 'id_btn_email','class' => 'btn btn-primary']);
							echo "</div>\n";
							echo '<div class="form-group">'."\n";
								echo form_label('&nbsp;Email Address to send PDF','email_pdf',['class' => 'control-label']).' ';
								echo form_input(['name'=> 'email_pdf','type' => 'text','id' => 'id_email_pdf','maxlength' => '60','size' => '25', 'class' => 'form-control']);
							echo "</div>\n";
						echo form_close();
					  ?>			
					</div>	
				</div>	
			</div>	
		</div>	
		<div class="row">
			<div class="col-md-6">
				<h4><span class="label label-default">Patient Information</span></h4>
				<table class="table table-bordered table-condensed">
					<tbody>
					<tr>
						<td>Date birth</td><td><?php echo date('M j, Y',strtotime($date_birth)) ?></td>
					</tr>
					<tr>
						<td>Address</td>
						<td>
							<?php echo $address_block; ?>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-6">
				<h4><span class="label label-default">Lab Information</span></h4>
				<table class="table table-bordered table-condensed"><tbody>
				<tr>
					<td>Lab Name</td> <td><?php echo $lab_name.(! empty($lab_location) ? ' ('.$lab_location.')' : '') ?></td>
				</tr>
				<tr>
					<td>Lab Operator</td> <td><?php echo $staff_name_first.' '.$staff_name_last ?></td>
				</tr>
				<tr>
					<td>Lab Operator Location</td> <td><?php echo $staff_location ?></td>
				</tr>
				</tbody>
				</table>
			</div>
		</div>	
		<div class="row">
			<div class="col-md-12">
				<?php if( empty($test_result_diagnosis) ): ?>
					<div class="text-danger">There are no test results to report.</div>
				<?php else: ?>
					<h4><span class="label label-default">Diagnosis</span></h4><br>
					<div style="width:750px">
					<?php echo $test_result_diagnosis ?>
					</div>
				<?php  endif; ?>
			</div>
		</div>
	</div>
</div>
