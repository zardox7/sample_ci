<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('address_block')){
	function address_block($arr){
		$str = '';
		if( ! empty($arr['address_1'])){ $str .= $arr['address_1'].'<br>'; } 
		if( ! empty($arr['address_2'])){ $str .= $arr['address_2'].'<br>'; } 
		$str .= $arr['city'];
		if( ! (empty($arr['city']) && empty($arr['state']))){ $str .= ', '; }
		elseif( ! empty($arr['postal_code'])){ $str .= ' '; }
		$str .= $arr['state'];
		if( ! (empty($arr['state']) && empty($arr['postal_code']))){ $str .= ' '; } 
		$str .= $arr['postal_code'];
		if( ! (empty($arr['city']) && empty($arr['state']) && empty($arr['postal_code']))){ $str .= '<br>'; }
		if( ! empty($arr['country'])){ $str .= $arr['country']; } 
		return $str;
	}
}
class Form_data {

	public function __construct() {
	  //  parent::__construct();
	}

	function generate_options($from,$to,$callback=false){
		$reverse=false;

		if($from>$to){
			$tmp=$from;
			$from=$to;
			$to=$tmp;
	
			$reverse=true;
		}
		$return_string=array();
		for($i=$from;$i<=$to;$i++){
			$return_string[]='
			<option value="'.$i.'">'.($callback?$callback($i):$i).'</option>
			';
		}

		if($reverse){
			$return_string=array_reverse($return_string);
		}
		return join('',$return_string);
	}

	function callback_month($month){
		return date('F',mktime(0,0,0,$month,1));
	}

	function format_date($date){
		$parts = explode('-',$date);
		return date('F j, Y',mktime(0,0,0,$parts[1],$parts[2],$parts[0]));
	}


}