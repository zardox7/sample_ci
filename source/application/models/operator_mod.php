<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Lab Operator User Models
 * 
 * @package Lab Operator website
 * @author 	Michael Collins
 */

/**
 * Operator_mod class is for the Model, all functions within the Operator Website
 */
class Operator_mod extends CI_Model {
	
	/**
	 * @property string $lab_table (etc) defines the long names for the tables
	 */
	private $lab_table='exp_mc_lab';
	private $patient_table='exp_mc_patient';
	private $staff_table='exp_mc_staff';
	private $test_result_table='exp_mc_test_result';
	private $test_table='exp_mc_test';

	function __construct(){
		parent::__construct();
	}
			   
	/**
	 * Compares form values to the values in the database to perform authentication 
	 *
	 * @param  string Email     
	 * @param  string Password     
	 * @return boolean to indicate if login is a success
	 */
	function login_operator($staff_email,$staff_password){
		$this->db->where(['staff_email' => $staff_email, 'staff_password' => $staff_password]);
		$query = $this->db->get($this->staff_table);
		if($query->num_rows() == 1){
			$this->session->set_userdata(['ID_staff' => $query->row()->ID_staff]);
			$this->session->set_userdata(['displayname' => $query->row()->staff_name_first.' '.$query->row()->staff_name_last]);
			$this->session->set_userdata(['email' => $query->row()->staff_email]);
			$this->session->set_userdata(['staff_password' => $query->row()->staff_password]);
			$this->input->set_cookie(
				[
					'name'   => 'staff_email',
					'value'  => $query->row(0)->staff_email,
					'expire' => '86500',
					'path'   => '/'
				]
			); 
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	 * helper - retrieves values from the database needed to populate option tags in lab test form dropdowns
	 *
	 * @param  - which dropdown the options are for     
	 * @return  - return an associative array of records
	 */
	function get_options($w='') {
		switch($w){
			case 'test':
				$this->db->select('ID_test AS id, concat(test_name,if(test_code = "", "", concat(" (",test_code,")"))) AS name');
				$this->db->from($this->test_table);
				$this->db->order_by('test_name','ASC');
				break;
			case 'lab':
				$this->db->select('ID_lab AS id, concat(lab_name,if(lab_location = "", "", concat(" (",lab_location,")"))) AS name');
				$this->db->from($this->lab_table);
				$this->db->order_by('lab_name','ASC');
				break;
			default:
				$this->db->select('ID_staff AS id, concat(staff_name_first," ",staff_name_last,if(staff_location = "", "", concat(" (",staff_location,")"))) AS name');
				$this->db->from($this->staff_table);
				$this->db->where('staff_role','operator');
				$this->db->order_by('staff_name_last','ASC');
				$this->db->order_by('staff_name_first','ASC');
				break;
		}
		$res_arr = $this->db->get()->result_array();
		return $res_arr;
	}

	/**
	 * Builds the HTML used in links can be used for any page with a list of records that is too long for that page
	 *
	 * @param array - config values specific to the pagination of records
	 * @return string - HTML to be insert where pagination links are needed
	 */
	function get_links($pass_arr){
		// returns a string, the html for links between pages of results
		$config = [
			'num_links' => 9,
			'full_tag_open' => '<ul id="id_page_list_container" class="pagination" style="margin:0;">',
			'full_tag_close' => '</ul>',
			'first_link' => '&laquo; First',
			'first_tag_open' => '<li>',
			'first_tag_close' => '</a></li>',
			'last_link' => 'Last &raquo;',
			'last_tag_open' => '<li>',
			'last_tag_close' => '</li>',
			'prev_link' => '&laquo; Previous',
			'prev_tag_open' => '<li>',
			'prev_tag_close' => '</li>',
			'next_link' => 'Next &raquo;',
			'next_tag_open' => '<li>',
			'next_tag_close' => '</li>',
			'cur_tag_open' => '<li class="active"><a >',
			'cur_tag_close' => '</a></li>',
			'num_tag_open' => '<li>',
			'num_tag_close' => '</li>'
		];
		foreach($pass_arr as $key => $value){
			$config[$key]=$value;
		}
		$this->load->library('pagination');
        $this->pagination->initialize($config);
       	$pagination_html = $this->pagination->create_links();
   		// Add a unique ID within the anchor tag, for jQuery to use as an selector 
   		$pag_arr = explode('<a ', $pagination_html);
		$i=0;
		foreach( $pag_arr as $value ){
			$pag_arr[$i] = str_replace('href="/', 'id="id_page_link_'.$i.'" offset="',$value);
			$i++;
		}
        $pagination_html = implode('<a ', $pag_arr);  
		return $pagination_html;
    }

	/**
	 * Build SQL statement, with Joins, and retrieve patient record data 
	 *
	 * @param integer offset the number of records to skip in the record set
	 * @param integer per_page the number of records total to return
	 * @param string patient_filter the user entered value on constraining the records shown
	 * @param string sort_field the user selected value for the field to sort on
	 * @param string sort_by the direction of the sort
	 * @return array the first item is the number of records (returned in the non-limited search), the second is the query result (array of objects)
	 */
	function get_page_data_patient($offset=0, $per_page, $patient_filter='', $sort_field='', $sort_by='ASC'){
		// load data
		if( empty($sort_field) ){
			$sort_field = 'name_last, name_first';
		}
		elseif( strpos($sort_field,'name_last') === false ){
			$sort_field=$sort_field.', name_last, name_first';
		}
		$this->db->select(['ID_patient','name_first','name_last','email','address_1','address_2','city','state','postal_code','country','gender','date_birth','patient_pass_code']);
		$this->db->order_by($sort_field,$sort_by);
		$this->db->limit($per_page,$offset);
		if( empty($patient_filter) ){
			$query = $this->db->get($this->patient_table);
		}
		else{
			$filter_arr = explode(' ',$patient_filter);
			foreach( $filter_arr AS $thisFilter ){
				$thisFilter = trim($thisFilter);
				$this->db->group_start();
				$this->db->or_like('name_first',$thisFilter,'both',true);
				$this->db->or_like('name_last',$thisFilter,'both',true);
				$this->db->or_like('city',$thisFilter,'both',true);
				$this->db->or_like('state',$thisFilter,'both',true);
				$this->db->or_like('email',$thisFilter,'both',true);
				$this->db->group_end();
			}
			$query = $this->db->get($this->patient_table);
		}
		$this->db->select('count(ID_patient) AS total_rows ');
		if( ! empty($patient_filter) ){
			foreach( $filter_arr AS $thisFilter ){
				$thisFilter = trim($thisFilter);
				$this->db->group_start();
				$this->db->or_like('name_first',$thisFilter,'both',true);
				$this->db->or_like('name_last',$thisFilter,'both',true);
				$this->db->or_like('city',$thisFilter,'both',true);
				$this->db->or_like('state',$thisFilter,'both',true);
				$this->db->or_like('email',$thisFilter,'both',true);
				$this->db->group_end();
			}
		}
		$total_rows = $this->db->get($this->patient_table)->row()->total_rows;
		if((int)$total_rows > 0){
			return ['total_rows' => $total_rows, 'query_obj' => $query->result()];
		}
		else{
			return false;
		}
	}

	/**
	 * Submit a database request to get the field data for a single patient
	 *
	 * @param integer $ID_patient ID of the patient record
	 * @return   CI result object  
	 */
	function patient_get($ID_patient){
		$this->db->where('ID_patient', $ID_patient);
		return $this->db->get($this->patient_table);
	}

	/**
	 * Submit the user entered data from patient data on the add/update form to the database
	 *
	 * @param array $patient_arr user form entries
	 * @return array the result, the patient ID (integer), and the patient passcode (string)
	 */
	function patient_save($patient_arr){
		$type='';
		if( array_key_exists('ID_patient', $patient_arr)) {
			$ID_patient = $patient_arr['ID_patient'];
			unset($patient_arr['ID_patient']);
		}
		else{
			$ID_patient = 0;
		}
		// Get any exisiting patient_pass_code, but if none (for adds) and incomplete patient records: Generate new. If existing, it does not need to be updated
		if((! array_key_exists('patient_pass_code', $patient_arr) ) || ( array_key_exists('patient_pass_code', $patient_arr) && empty($patient_arr['patient_pass_code']))) {
			$this->load->helper('string');
			$patient_pass_code = random_string('alnum', 16);
			$patient_arr['patient_pass_code'] = $patient_pass_code;
		}
		else{
			$patient_pass_code=$patient_arr['patient_pass_code'];
			unset($patient_arr['patient_pass_code']);
		}
		// ensure dates correctly entered to database

		if( empty($ID_patient) ) {
			$type='add';
			$patient_arr['date_created'] = date("Y-m-d H:i:s");
			$res_bool = $this->db->set($patient_arr)->insert($this->patient_table);
			if($res_bool === true) {
				$ID_patient = $this->db->insert_id();
			}
			else{
				$type='fail';
			}			
		}
		else{
			$type='update';
			$this->db->where('ID_patient', $ID_patient );
			$res_bool = $this->db->set($patient_arr)->update($this->patient_table);
			if($res_bool === false){
				$type='fail';
				$ID_patient = 0;
			}
		}
		if(! $this->db->error()['code'] == 0) { fire_log( $this->db->error() );	}
		
		return [
			'type' => $type,
			'ID_patient' => $ID_patient,
			'patient_pass_code' => $patient_pass_code
		];
	}
	
	/**
	 * Perform the database request to delete a Patient record, and all records that are related by that Patient ID
	 *
	 * @param   integer $ID_test_result ID of the test result record
	 * @param   string $w Flag value to indicate 'all' (delete patient record and all test result records for that patient) or 'po' (patient only)
	 * @return boolean 
	 */
	function patient_delete($ID_patient,$w){
		$affected_rows=0;
		if( ! empty($ID_patient) ) {
			if($w == 'all'){
				$this->db->where('FKID_patient', $ID_patient);
				$this->db->delete($this->test_result_table);
			}
			$this->db->where('ID_patient', $ID_patient);
			$this->db->delete($this->patient_table);
			$affected_rows = $this->db->call_function('affected_rows',$this->db->conn_id);
		}
		return $affected_rows;
	}
	
	/**
	 * The SELECT portion of the SQL statement
	 *
	 * @param none
	 * @return string 
	 */
	function get_select_test_result_table(){
		$select_str = <<<'STR'
tr.ID_test_result,
tr.FKID_patient,
tr.FKID_test,
tr.FKID_staff_lab,
tr.FKID_lab,
tr.date_test_result_request,
tr.date_test_result_service,
tr.test_result_diagnosis,
tr.isEmailPatientSent,
p.name_first,
p.name_last,
p.email,
t.test_name,
s.staff_name_last,
s.staff_name_first,
l.lab_name
STR;
		return $select_str;
	}
	
	/**
	 * The FROM portion of the SQL statement
	 *
	 * @param none
	 * @return string 
	 */
	function get_from_test_result_table(){
		$from_str = <<<STR
{$this->test_result_table} as tr 
JOIN {$this->patient_table} as p ON (tr.FKID_patient=p.ID_patient) 
LEFT JOIN {$this->lab_table} as l ON (tr.FKID_lab=l.ID_lab) 
LEFT JOIN {$this->staff_table} as s ON (tr.FKID_staff_lab=s.ID_staff) 
LEFT JOIN {$this->test_table} as t ON (tr.FKID_test=t.ID_test)
STR;
		return $from_str;
	}
	
	/**
	 * Build SQL statement, with Joins, and retrieve test results record data 
	 *
	 * @param integer offset the number of records to skip in the record set
	 * @param integer per_page the number of records total to return
	 * @param integer FKID_patient the ID of the current patient
	 * @param integer test_result_filter number of months past
	 * @param string sort_field the user selected value for the field to sort on
	 * @param string sort_by the direction of the sort
	 * @return array the first item is the number of records (returned in the non-limited search), the second is the query result (array of objects)
	 */
	function get_page_data_test_result($offset=0, $per_page, $FKID_patient, $test_result_filter, $sort_field='', $sort_by='ASC'){
		$test_result_filter = (int)$test_result_filter;
		// load data
		if( empty($sort_field) ){
			$sort_field = 'date_test_result_service';
		}
		if($test_result_filter === 999){
			$date_test_result_service = '';
		}
		elseif($test_result_filter === 1){
			$date_test_result_service = date('Y-m-d', strtotime('-1 months'));
		}
		elseif($test_result_filter === 12){
			$date_test_result_service = date('Y-m-d', strtotime('-12 months'));
		}
		else{
			$date_test_result_service = date('Y-m-d', strtotime('-6 months'));
		}
		$this->db->select( $this->get_select_test_result_table() );
		$this->db->from( $this->get_from_test_result_table() );
		$this->db->where('tr.FKID_patient', $FKID_patient);
		if( ! empty($date_test_result_service) ){
			$this->db->where('tr.date_test_result_service >=', $date_test_result_service);
		}
		$this->db->order_by($sort_field,$sort_by);
		$this->db->limit($per_page,$offset);
		$query = $this->db->get();
		// build and run query again to capture count
		$this->db->select('count(tr.ID_test_result) AS total_rows ');
		$this->db->from( $this->get_from_test_result_table() );
		$this->db->where('tr.FKID_patient', $FKID_patient);
		if( ! empty($date_test_result_service) ){
			$this->db->where('tr.date_test_result_service >=', $date_test_result_service);
		}
		$total_rows = $this->db->get()->row()->total_rows;
		if((int)$total_rows > 0){
			return ['total_rows' => $total_rows, 'query_obj' => $query->result()];
		}
		else{
			return false;
		}
	}

	/**
	 * Uses the test result ID to lookup all test result fields from the database
	 *
	 * @param   integer $ID_test_result ID of the test result record
	 * @return   CI result object  
	 */
	function labtest_result_get($ID_test_result){
		$this->db->select($this->get_select_test_result_table());
		$this->db->from($this->get_from_test_result_table());
		$this->db->where('tr.ID_test_result', $ID_test_result);
		return $this->db->get($this->test_result_table);
	}
		
	/**
	 * Submit the user entered data from test result data on the add/update form to the database
	 *
	 * @param array test_result_arr array of user entries
	 * @return array the result, the test result ID (integer), and the patient ID (integer)
	 */
	function labtest_result_save($test_result_arr){
		$result='';
		$FKID_patient=$test_result_arr['FKID_patient'];
		if( array_key_exists('ID_test_result', $test_result_arr)) {
			$ID_test_result = $test_result_arr['ID_test_result'];
			unset($test_result_arr['ID_test_result']);
		}
		else{
			$ID_test_result = '';
		}
		if( empty($FKID_patient) ) {
			$result='fail';
		}
		else{
			if( empty($ID_test_result) ) {
				$result='add';
				$test_result_arr['date_created'] = date("Y-m-d H:i:s");
				$res_bool = $this->db->set($test_result_arr)->insert($this->test_result_table);
				if($res_bool === true) {
					$ID_test_result = $this->db->insert_id();
				}
				else{
					$result='fail';
				}			
			}
			else{
				$result='update';
				$this->db->where('ID_test_result', $ID_test_result );
				$res_bool = $this->db->set($test_result_arr)->update($this->test_result_table);
				if($res_bool === false) {
					$result='fail';
				}
			}
		}
		if(! $this->db->error()['code'] == 0) { fire_log( $this->db->error() );	}
	
		return [
			'result' => $result,
			'ID_test_result' => $ID_test_result,
			'FKID_patient' => $FKID_patient
		];
	}
	
	/**
	 * Perform the database request to delete a record
	 *
	 * @param   integer $ID_test_result ID of the test result record
	 * @return boolean (as a string, since this is used with AJAX)
	 */
	function labtest_result_delete($ID_test_result){
		$affected_rows=0;
		if( ! empty($ID_test_result)) {
			$this->db->where('ID_test_result', $ID_test_result);
			$this->db->delete($this->test_result_table);
			$affected_rows = $this->db->call_function('affected_rows',$this->db->conn_id);
		}
		return $affected_rows;
	}
	
	/**
	 * Perform the database request to find all field values to display on a report
	 *
	 * @param   integer $ID_test_result ID of the test result record
	 * @return   CI result object  
	 */
	function patient_test_result_report($ID_test_result){
		if(empty($ID_test_result)) {
			return false;
		}
		else{
			$this->db->select('tr.*,p.*,l.*,s.*,t.*');
			$this->db->from( $this->get_from_test_result_table() );
			$this->db->where('tr.ID_test_result', $ID_test_result);
			return $this->db->get()->row();
		}
	}
	
	/**
	 * Run a maintenance SQL statement to delete test result records that have no parent patient record
	 *
	 * @return   CI result object  
	 */
	function maintenance_delete_orphan_test_result_records(){
		$this->db->query("delete {$this->test_result_table}  FROM {$this->test_result_table} left join {$this->patient_table} ON ({$this->test_result_table}.FKID_patient={$this->patient_table}.ID_patient) WHERE {$this->patient_table}.ID_patient IS NULL");
		return $this->db->call_function('affected_rows',$this->db->conn_id);
	}
}
?>