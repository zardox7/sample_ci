<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Example extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->local_stylesheets = array( 'example.css' ); 
		$this->local_javascripts = array( 'example.js'); 
	}

	public function index() {
		$content = $this->load->view('example',NULL,true);
		$this->render($content);
	}
}
