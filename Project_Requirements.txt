Michael Collins

Crossover Axis Pathology Lab Reporting System

Requirements 

Create a pathology lab reporting web application where medical test result reports can be published to the patients.

Objective

Create a pathology lab reporting system, which can be used to publish medical test result reports to patients.

Functional Specifications

Create a pathology lab reporting web application where medical test result reports can be published to the patients.
	
	• Operator users should be able to log in to the system to perform following privileged tasks. Patients cannot access these pages. 
	
		> Reports CRUD (Multiple tests and results in each report) 
	
		> Patients CRUD (including pass code)
	
	• Lab sends a text message to the patient with a pass code to log in (out of scope). 
	
	• Patient user could log in using his name (auto complete field) and pass code sent to him. And then can do the following: 
		> Display list of his reports. 
		> Display a report details as a page. 
		> Export a report as PDF 
		> Mail a report as PDF

Technical Specifications

The following list of technical specifications should be adhered to:

• Use MVC design 
• Use MySQL database. Create the needed tables. 
• Use a PDF export library. 
• Use PHP Mailer as mailing library 
• Apply input validations and constraints wherever necessary to create a stable application. 

To be evaluated

• The quality of the output (functionality) 
• Code quality and completeness 
• Technologies applied 
• Extra validations and assumptions which are not described 
• Add missing requirements to the implementation, according to your experience.

Delivery / What to submit

Application Demo

Record the demonstration of the web application using Wink. Do not upload the video. Save it to your localmachine. 

Database script

Create a single SQL script file to create the database, its schema and any stored procedure you may use. 

Readme

Create a txt file with the following information

• Steps to create and initialize the database. 
• Steps to prepare the source code to build/run properly 
• Any assumptions made and missing requirements that are not covered in the specifications 
• Any feedback you may wish to give about improving the assignment.

Design diagrams

Create a doc file containing the following information and diagrams

• List of technologies and design patterns used. 
• An overall activity diagram 
• An overall component interaction diagram

Deliverable Structure

Delivery for this assignment should consist of an archive named - Software Engineer -PHP.zip containing the following

• Source code/project 
• Wink recording, download version 2 from Wink, render the video to swf format 
• Readme.txt containing the instructions to configure and run the application, notes and feedback 
• Design.doc with needed diagrams

Structure of the resulting zip file should be of the following format 
{your_name} - Software Engineer - PHP.zip 
{your_name} - Software Engineer - PHP.zip \Readme.txt 
{your_name} - Software Engineer - PHP.zip \Design.doc 
{your_name} - Software Engineer - PHP.zip \Wink\ <<< this folder should contain the wink recording 
{your_name} - Software Engineer - PHP.zip \Source\ <<< this folder should contain the complete sourcecode for the project(s) 